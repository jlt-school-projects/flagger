package com.fr.juleslaurent.flagger.ui.issuecreation

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.location.Location
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.fr.juleslaurent.flagger.R
import android.view.Menu

import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.fr.juleslaurent.flagger.model.Domain
import android.widget.ArrayAdapter
import android.widget.ImageView
import com.fr.juleslaurent.flagger.ui.login.afterTextChanged
import kotlinx.android.synthetic.main.activity_issue_creation.*
import android.provider.MediaStore


class IssueCreationActivity : AppCompatActivity() {
    private lateinit var issueCreationViewModel: IssueCreationViewModel
    companion object {
        val SELECT_PICTURE_REQUEST_CODE = 11;
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_issue_creation)

        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_clear_white_24dp);
        supportActionBar?.setDisplayHomeAsUpEnabled(true);



        issueCreationViewModel =
            ViewModelProviders.of(this).get(IssueCreationViewModel::class.java)


        val location: Location = intent.extras?.get("location") as Location;
        issueCreationViewModel.setLocation(location);

        issueCreationViewModel.availableDomains.observe(this, Observer {
            this.populateDomains(it);

        })

        issueCreationViewModel.images.observe(this, Observer {
            val imageView = ImageView(this);
            if(it.count() > 0) {
                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, it.last())
                imageView.setImageBitmap(bitmap);

                image_preview_layout.addView(imageView,0)

                val scale = resources.displayMetrics.density;
                imageView.layoutParams.height = (100 * scale + 0.5f).toInt()
                imageView.layoutParams.width = (100 * scale + 0.5f).toInt()
            }

        })


        issue_description_edit.afterTextChanged {
            issueCreationViewModel.issueToCreate.description = it
        }

        issue_title_edit.afterTextChanged {
            issueCreationViewModel.issueToCreate.title = it
        }

        domain_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                issueCreationViewModel.issueToCreate.domain = null;
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                issueCreationViewModel.issueToCreate.domain = (parent?.getItemAtPosition(position) as Domain).id;
            }
        }




        this.issueCreationViewModel.getDomains();
    }



    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.valid_cancel_menu, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.getItemId()


         if (id == R.id.action_validate) {
             Toast.makeText(this, "Creating issue", Toast.LENGTH_SHORT).show()
             issueCreationViewModel.createIssue(this);
            return true
        } else if (android.R.id.home == id){
             finish();
         }

        return super.onOptionsItemSelected(item)

    }

    override fun onBackPressed() {
        Toast.makeText(this, "NAcl", Toast.LENGTH_SHORT).show()

        super.onBackPressed()
    }

    fun populateDomains(domains: MutableList<Domain>) {
        val adapter = ArrayAdapter<Domain>(
            applicationContext,
            android.R.layout.simple_spinner_dropdown_item,
            domains
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        domain_spinner.adapter = adapter;
    }

    fun selectImage(v: View) {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode:Int, resultCode:Int, data:Intent?) {
        if(requestCode == SELECT_PICTURE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val fullPhotoUri: Uri = data?.data!!;
            Log.e("MYFILE",fullPhotoUri.normalizeScheme().toString())
            issueCreationViewModel.addImage(fullPhotoUri);
        }
        super.onActivityResult(requestCode,resultCode,data);

    }


}

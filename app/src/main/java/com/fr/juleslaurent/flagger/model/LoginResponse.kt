package com.fr.juleslaurent.flagger.model

import java.io.Serializable

class LoginResponse : Serializable {
    var token: String? = null
}
package com.fr.juleslaurent.flagger.ui.login

import android.content.Context
import android.util.Log
import androidx.lifecycle.ViewModel
import android.util.Patterns
import androidx.lifecycle.MutableLiveData
import com.fr.juleslaurent.flagger.R
import com.fr.juleslaurent.flagger.model.Credentials
import com.fr.juleslaurent.flagger.model.Issue
import com.fr.juleslaurent.flagger.model.LoginResponse
import com.fr.juleslaurent.flagger.service.ApiInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.math.log

class LoginViewModel() : ViewModel() {


    val formValid = MutableLiveData<Boolean>();
    var credentials: Credentials = Credentials();
    val error : MutableLiveData<MutableMap<String, String>> = MutableLiveData<MutableMap<String, String>>().apply { value = mutableMapOf() }

    val loginSuccess = MutableLiveData<Boolean>();
    fun login(username: String, password: String) {
        credentials.username = username;
        credentials.password = password;
        ApiInterface.instance.login(username, password).enqueue(onLoginResponse)
    }


    private val onLoginResponse = object : Callback<LoginResponse> {
        override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
            if (response.body() != null && response.code() == 200) {
                val loginResponse = response.body() as LoginResponse;
                if (loginResponse.token != null) {
                    credentials.token = loginResponse.token;
                    loginSuccess.value = true;
                }
            }else if (response.code() == 401) {
                loginSuccess.value = false;
            }
        }

        override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
            Log.e("HTTP FAILURE", t.message!!)
        }
    }



    fun loginDataChanged(username: String, password: String) {
        formValid.value = false;
        error.value?.clear();
        if (!isUserNameValid(username)) {
            error.value?.set("username","Username is invalid");
            error.postValue(error.value);
        } else {
            formValid.value = true;
        }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return  Patterns.EMAIL_ADDRESS.matcher(username).matches()
    }
}

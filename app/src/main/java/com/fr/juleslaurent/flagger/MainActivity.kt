package com.fr.juleslaurent.flagger

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker
import com.fr.juleslaurent.flagger.ui.login.LoginActivity
import com.auth0.android.jwt.JWT





class MainActivity : AppCompatActivity() {
    val LOCATION_PERMISSION_REQUEST_CODE = 1;
    val STORAGE_PERMISSION_REQUEST_CODE = 2;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications
            )
        )



        if (PermissionChecker.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PermissionChecker.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
        }

        if (PermissionChecker.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PermissionChecker.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                STORAGE_PERMISSION_REQUEST_CODE
            )
        }


        //TODO Implement account manager
//
//        val am: AccountManager = AccountManager.get(this) // "this" references the current Context
//
//        val accounts: Array<out Account> = am.getAccountsByType("com.fr.juleslaurent.flagger");
//
//        if(accounts.count() == 0) {
//
//        }

        val sharedPreferences = getSharedPreferences(getString(R.string.shared_preferences_file), Context.MODE_PRIVATE)!!

        if(!sharedPreferences.contains("token")) {
            startLogin()
        } else {
            val token = sharedPreferences.getString("token","");
            val jwt = JWT(token!!)
            if(jwt.isExpired(10)) {
                startLogin()
            }
        }

        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }




    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {

        if(grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            //TODO QUIT
        }
    }

    fun startLogin()  {
        val intent = Intent(this, LoginActivity::class.java);
        startActivityForResult(intent, LoginActivity.REQUEST_CODE_REQUIRE_LOGIN);
    }

}

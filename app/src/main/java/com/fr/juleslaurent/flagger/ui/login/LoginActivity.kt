package com.fr.juleslaurent.flagger.ui.login

import android.accounts.AccountAuthenticatorActivity
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.fr.juleslaurent.flagger.R
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity :  AppCompatActivity() {
    companion object {
         val REQUEST_CODE_REQUIRE_LOGIN = 2;
    }
    private lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)


        val username = findViewById<EditText>(R.id.username)
        val password = findViewById<EditText>(R.id.password)
        val login = findViewById<Button>(R.id.login)
        val loading = findViewById<ProgressBar>(R.id.loading)
        val sharedPreferences = getSharedPreferences(getString(R.string.shared_preferences_file), Context.MODE_PRIVATE)!!

        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)

        loginViewModel.formValid.observe(this, Observer { value ->
            login.isEnabled = value;
         })

        loginViewModel.error.observe(this, Observer { value ->
            if(value?.get("username") != null) {
                username.error = value.get("username");
            }
        })

        loginViewModel.loginSuccess.observe(this, Observer { value ->
            loading.visibility = View.INVISIBLE;
            if(!value) {
                showLoginFailed(R.string.login_failed);
                password.setText("");
            } else {

                val credentials = loginViewModel.credentials;
                sharedPreferences.edit().putString("token",credentials.token)
                    .putString("username",credentials.username)
                    .putString("password",credentials.password)
                    .apply();


                finish();
            }

        })


        username.afterTextChanged {
            loginViewModel.loginDataChanged(
                username.text.toString(),
                password.text.toString()
            )
        }

        password.apply {
            afterTextChanged {
                loginViewModel.loginDataChanged(
                    username.text.toString(),
                    password.text.toString()
                )
            }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE ->
                        loginViewModel.login(
                            username.text.toString(),
                            password.text.toString()
                        )
                }
                false
            }

            login.setOnClickListener {
                loading.visibility = View.VISIBLE
                loginViewModel.login(username.text.toString(), password.text.toString())
            }
        }

        if(sharedPreferences.contains("username") && sharedPreferences.contains("password")) {
            loginViewModel.login(sharedPreferences.getString("username","")!!,sharedPreferences.getString("password","")!!)
        }
    }

    private fun showLoginFailed(@StringRes errorString: Int) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })
}

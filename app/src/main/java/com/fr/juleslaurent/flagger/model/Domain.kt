package com.fr.juleslaurent.flagger.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Domain : Serializable {
    @SerializedName("_id")
    var id: String? = null;
    var name: String? = null;
//    var owner: User? = null;
//    var admins: ArrayList<User> = arrayListOf<User>()

    override fun toString(): String {
        return this.name!!;
    }
}
package com.fr.juleslaurent.flagger.ui.issuecreation

import android.content.Context
import android.location.Location
import android.net.Uri
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.fr.juleslaurent.flagger.model.Domain
import com.fr.juleslaurent.flagger.model.Issue
import com.fr.juleslaurent.flagger.model.IssuePostPayload
import com.fr.juleslaurent.flagger.model.Point
import com.fr.juleslaurent.flagger.service.ApiInterface
import com.fr.juleslaurent.flagger.util.PathUtils
import okhttp3.MediaType
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import okhttp3.RequestBody
import java.io.File


class IssueCreationViewModel : ViewModel() {

    val issueToCreate: IssuePostPayload = IssuePostPayload();
    val availableDomains: MutableLiveData<MutableList<Domain>> =  MutableLiveData<MutableList<Domain>>()
    val images: MutableLiveData<MutableList<Uri>> = MutableLiveData<MutableList<Uri>>().apply { value = arrayListOf() }

    fun createIssue(context: Context) {
        Log.i("ISSUE_CREATION","BEGIN");
        ApiInterface.instance.createIssue(issueToCreate, prepareImagesPart(context)).enqueue(this.onIssueCreated);
    }

    fun getDomains() {
        ApiInterface.instance.getDomains().enqueue(this.onDomainsReceived);
    }

    fun addImage(uri: Uri) {
        images.value?.add(uri);
        images.postValue(images.value!!);
    }

    fun setLocation(location: Location) {
        val point = Point();
        point.coordinates.set(0,location.latitude);
        point.coordinates.set(1, location.longitude);
        issueToCreate.location = point;
    }

    fun prepareFilePart(context: Context, uri: Uri, partName: String) : MultipartBody.Part {

        val file = File(PathUtils.getPath(context,uri));

        // create RequestBody instance from file
        val requestFile = RequestBody.create(
            MediaType.parse(context.contentResolver.getType(uri)!!),
            file
        )

        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile)
    }

    fun prepareImagesPart(context: Context):List<MultipartBody.Part>  {
         val parts: MutableList<MultipartBody.Part> = arrayListOf();
         for((index, image) in images.value?.withIndex()!!) {
             parts.add(prepareFilePart(context, image, "images"))
         }
        Log.i("ISSUE_CREATION","PARTS READY");

        return parts;
    }

    private val onIssueCreated = object : Callback<Issue> {
        override fun onResponse(call: Call<Issue>, response: Response<Issue>) {
            if (response.body() != null && response.code() == 200) {
                val receivedIssues = response.body() as Issue;
            }
        }

        override fun onFailure(call: Call<Issue>, t: Throwable) {
            Log.e("HTTP FAILURE", t.message!!)
        }
    }

    private val onDomainsReceived = object : Callback<List<Domain>> {
        override fun onResponse(call: Call<List<Domain>>, response: Response<List<Domain>>) {
            if (response.body() != null && response.code() == 200) {
                availableDomains.value = response.body() as MutableList<Domain>;

            }
        }

        override fun onFailure(call: Call<List<Domain>>, t: Throwable) {
            Log.e("HTTP FAILURE", t.message!!)
        }
    }



}
package com.fr.juleslaurent.flagger.ui.issuemap

import android.content.Intent
import android.graphics.BitmapFactory
import android.location.Location
import android.location.LocationProvider
import android.os.Bundle
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.fr.juleslaurent.flagger.R

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import android.widget.Toast
import com.fr.juleslaurent.flagger.MainActivity
import com.fr.juleslaurent.flagger.model.Issue
import com.fr.juleslaurent.flagger.ui.issuecreation.IssueCreationActivity
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_issuemap.*


class IssueMapFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMyLocationClickListener{

    private lateinit var issueMapViewModel: IssueMapViewModel
    private var _map: GoogleMap? = null;
    private lateinit var fusedLocationClient: FusedLocationProviderClient;
    private lateinit var locationCallback: LocationCallback



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        issueMapViewModel =
            ViewModelProviders.of(this).get(IssueMapViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_issuemap, container, false)

        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        report_button.setOnClickListener {
            this.onAddButtonClick();
        };


        val mapFragment =
            childFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment

        issueMapViewModel.issues.observe(this, androidx.lifecycle.Observer {issues ->
            showIssuesOnMap(issues);
        })

        issueMapViewModel.currentLocation.observe(this, androidx.lifecycle.Observer {location ->
            this.issueMapViewModel.getIssues(location);
        })

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.requireActivity())


        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                onMyLocationResult(locationResult);
            }
        }

        mapFragment.getMapAsync(this);
    }


    fun onMyLocationResult(locationResult: LocationResult) {
        for (location in locationResult.locations){
            issueMapViewModel.locationChanged(location);
            centerMapOn(location);
        }
    }

    fun centerMapOn(location: Location) {
        this._map?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location.latitude, location.longitude),15f));
    }


    fun showIssuesOnMap(issues: MutableList<Issue>) {
        if(_map != null) {
            for(issue in issues) {
                val position = LatLng((issue.location?.coordinates?.get(0)) as Double, (issue.location?.coordinates?.get(1)) as Double)
                _map?.addMarker(
                    MarkerOptions().position(position)
                        .title(issue.title)
                )
            }
        }
    }

    override fun onMapReady(map: GoogleMap?) {

        this._map = map;
        this._map?.setOnMyLocationButtonClickListener(this);
        this._map?.setOnMyLocationClickListener(this);
        this._map?.isMyLocationEnabled = true;

        val locationRequest = LocationRequest.create()?.apply {
            interval = 10000
            fastestInterval = 5000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());

    }

    override fun onMyLocationButtonClick(): Boolean {
        Toast.makeText(activity,"My location button clicked", Toast.LENGTH_SHORT).show();
        return false;
    }

    override fun onMyLocationClick(location: Location) {
        Toast.makeText(activity, "Current location:\n$location", Toast.LENGTH_LONG).show();
    }

    fun onAddButtonClick() {
        val intent = Intent(context, IssueCreationActivity::class.java);
        intent.putExtra("location",issueMapViewModel.currentLocation.value);
        startActivity(intent);
    }

}
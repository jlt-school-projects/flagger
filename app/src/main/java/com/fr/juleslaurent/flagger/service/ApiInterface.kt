package com.fr.juleslaurent.flagger.service

import com.fr.juleslaurent.flagger.model.Domain
import com.fr.juleslaurent.flagger.model.Issue
import com.fr.juleslaurent.flagger.model.IssuePostPayload
import com.fr.juleslaurent.flagger.model.LoginResponse
import com.google.gson.GsonBuilder
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ApiInterface {
    @GET("/issues")
    fun getIssues() : Call<List<Issue>>

    @POST("/issues")
    @Multipart
    fun createIssue(@Part("issue") issue: IssuePostPayload, @Part files: List<MultipartBody.Part>) : Call<Issue>


    @GET("/domains")
    fun getDomains() : Call<List<Domain>>

    @GET("/login")
    fun login(@Query("email") username: String, @Query("password") password:String) : Call<LoginResponse>

    companion object {
        val instance: ApiInterface by lazy {
            val gsonBuilder = GsonBuilder();
            val gson = gsonBuilder.create();
            val retrofit = Retrofit.Builder()
                .baseUrl("https://flagger.juleslaurent.fr")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
            retrofit.create(ApiInterface::class.java)
        }
    }
}
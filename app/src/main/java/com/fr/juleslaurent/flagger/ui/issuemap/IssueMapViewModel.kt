package com.fr.juleslaurent.flagger.ui.issuemap

import android.location.Location
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import com.fr.juleslaurent.flagger.model.Issue
import com.fr.juleslaurent.flagger.service.ApiInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class IssueMapViewModel : ViewModel() {

    val issues : MutableLiveData<MutableList<Issue>> = MutableLiveData<MutableList<Issue>>();
    val currentLocation : MutableLiveData<Location> = MutableLiveData();


    fun getIssues(location: Location?) {
        ApiInterface.instance.getIssues().enqueue(this.onIssueReceived);
    }

    fun locationChanged(location: Location) {
        this.currentLocation.value = location;
    }

    private val onIssueReceived = object : Callback<List<Issue>> {
        override fun onResponse(call: Call<List<Issue>>, response: Response<List<Issue>>) {
            if (response.body() != null && response.code() == 200) {
                val receivedIssues = response.body() as MutableList<Issue>;
                issues.value = receivedIssues;

            }
        }

        override fun onFailure(call: Call<List<Issue>>, t: Throwable) {
            Log.e("HTTP FAILURE", t.message!!)
        }
    }


    
}
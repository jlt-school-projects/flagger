package com.fr.juleslaurent.flagger.model

import java.io.Serializable

class Credentials : Serializable {
    var token: String? = null
    var username: String? = null
    var password: String? = null
}
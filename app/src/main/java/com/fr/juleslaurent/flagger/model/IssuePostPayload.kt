package com.fr.juleslaurent.flagger.model

import java.io.Serializable

class IssuePostPayload : Serializable {
    var title: String? = null;
    var description: String? = null;
    var images: ArrayList<String> = arrayListOf<String>()
    var reporter: User? = null;
    var location : Point? = null;
    var domain: String? = null;
}
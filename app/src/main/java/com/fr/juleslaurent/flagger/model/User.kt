package com.fr.juleslaurent.flagger.model

import java.io.Serializable

class User : Serializable {
    var first_name: String? = null
    var last_name: String? = null
    var email: String? = null

}